import { toSimpleSchema, attachFormBuilderSchema } from './both/helpers'

FormBuilder = (function() {
  return {
    Collections: { DynamicForms },
    toSimpleSchema,
    attachFormBuilderSchema,
  }
})

FormBuilder = new FormBuilder()
