Package.describe({
  name: 'abate:formbuilder',
  version: '0.0.1',
  summary: 'Wrapper for formBuilder.online',
  git: 'git@gitlab.com:abate/meteor-formbuilder.git',
  documentation: 'README.md',
})

Npm.depends({
  formBuilder: '2.9.8',
  // "meteor-node-stubs": "0.2.4"
})

Package.onUse((api) => {
  api.versionsFrom('1.4')

  api.use([
    'mongo',
    'ecmascript',
    'tmeasday:check-npm-versions',
    'underscore',
    'check',
  ], ['client', 'server'])

  api.use([
    'templating',
  ], 'client')

  api.add_files([
    'both/collections.js',
    'both/helpers.js',
    'api.js',
  ], ['client', 'server'])

  api.add_files([
    'server/methods/api.js',
    'server/publications.js',
  ], ['server'])

  api.add_files([
    'client/formbuilder.html',
    'client/formbuilder.js',
    'client/formdisplay.html',
    'client/formdisplay.js',
  ], ['client'])

  api.export(['FormBuilder'])
})
