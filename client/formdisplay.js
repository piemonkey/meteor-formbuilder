import { moment } from 'meteor/momentjs:moment';

Template.formBuilderDisplay.onCreated(function (){
  const template = this;
  template.subscribe('FormBuilder.dynamicForms');
});

Template.formBuilderDisplay.helpers({
  fieldTemplate: function (type) {
    switch (type) {
      case 'select': return "basicFormDisplaySelect";
      case 'checkbox-group': return "basicFormDisplayInputGroup";
      case 'radio-group': return "basicFormDisplayInputGroup";
      case 'textarea': return "basicFormDisplayTextarea";
      case 'date': return "basicFormDisplayDate";
      default: return "basicFormDisplayInput";
    }
  },
  equal: function (a,b) { return (a == b);},
  schema: function () {
    var name = Template.currentData().formName;
    var form = DynamicForms.findOne({name:name});
    if (form) {
      return toSimpleSchema(form);
    }
  },
  fields: function() {
    var name = Template.currentData().formName;
    var form = Template.currentData().form;
    var data = DynamicForms.findOne({name:name});
    if (data && form) {
      return _.map(data.form, function(x){
        return _.extend(x,{field: x.name, value: form[x.name]});
      });
    }
  },
});

Template.basicFormDisplayInputGroup.helpers({
  checked: function (value) { return true;},
  atLeastOne: function (values) {
    return _.reduce(values,(v,a) => { return (v && a) ;},false);
  }
});

Template.basicFormDisplaySelect.helpers({
  selected: function (value) { return true;}
});

Template.basicFormDisplayDate.helpers({
  formatDate: function (value) {
    return moment(value).format('MMMM Do YYYY');
  }
});
