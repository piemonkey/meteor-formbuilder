import 'formBuilder/dist/form-builder.min.js'
import 'jquery-ui-sortable'
import 'formBuilder'

const SSTypes = ['String', 'Number', 'Boolean', 'Object', 'Date']

Template.formBuilder.onCreated(function onCreated() {
  const template = this
  const filterField = {
    label: 'Enable Filter',
    type: 'checkbox',
    // XXX I've to figure out how to add some kind of inline help text XXX
    // inlineDesc: "This field can be used to build a multi selection interface",
    // access: "This field can be used to build a multi selection interface"
  }
  const groupField = {
    label: 'Group Label',
    type: 'text',
  }
  const groupFieldHelp = {
    label: 'Group Help',
    type: 'text',
  }
  const additionalFields = { group: groupField, groupHelp: groupFieldHelp }
  template.options = {
    showActionButtons: false,
    dataType: 'json',
    disableFields: ['autocomplete', 'button', 'header', 'hidden', 'paragraph'],
    controlOrder: ['text', 'textarea', 'number', 'checkbox-group', 'radio-group', 'select', 'date'],
    disabledAttrs: ['multiple', 'access', 'other', 'toggle', 'subtype'],
    typeUserAttrs: {
      // checkbox: { filter: filterField, group : groupField},
      'checkbox-group': additionalFields,
      'radio-group': additionalFields,
      text: additionalFields,
      textarea: additionalFields,
      select: additionalFields,
      date: additionalFields,
      number: {
        group: groupField,
        groupHelp: groupFieldHelp,
        isArray: {
          label: 'Array of Values',
          type: 'checkbox',
        },
        jsType: {
          label: 'Simple-schema type',
          options: SSTypes,
        },
      },
    },
  }
})

Template.formBuilder.onRendered(function onRendered() {
  const template = this
  const sub = template.subscribe('FormBuilder.dynamicForms')
  const formWrap = $('.build-wrap')
  // if the user set a Name for the form, we keep using it
  template.originalName = Template.currentData().formUid.get()
  template.autorun(() => {
    if (sub.ready()) {
      const name = Template.currentData().formUid.get()
      const form = DynamicForms.findOne({ name })
      if (form) {
        template.options.formData = JSON.stringify(form.form)
      }
      if (!template.formBuilder) {
        template.formBuilder = formWrap.formBuilder(template.options)
      } else {
        const actions = template.formBuilder.actions
        actions.setData(template.options.formData)
      }
    }
  })
})

Template.formBuilder.helpers({
  name() {
    return Template.currentData().formUid.get()
  },
  removeButton() {
    return Template.currentData().removeButton
  },
  exportButton() {
    return Template.currentData().exportButton
  },
})

Template.formBuilder.events({
  'click #remove-form': function e(event, template) {
    event.preventDefault()
    const name = $(event.target).val()
    Meteor.call(
      'FormBuilder.dynamicForms.remove', name,
      (err) => {
        if (err) {
          alert(err)
        } else {
          const actions = template.formBuilder.actions
          template.options.formData = JSON.stringify({})
          actions.clearFields()
        }
      },
    )
  },
  'click #clear-all': function e(event, template) {
    event.preventDefault()
    const actions = template.formBuilder.actions
    Template.currentData().formUid.set(Template.instance().originalName)
    template.options.formData = JSON.stringify({})
    actions.clearFields()
  },
  'click #show-data': function e(event, template) {
    template.formBuilder.actions.showData()
  },
  'click #form-builder-save': function e(event, template) {
    event.preventDefault()
    const sel = {}
    // const newname = template.$(event.target).val()
    sel.name = template.$(event.target).val()
    if (!sel.name) { sel.name = template.$('#form-machine-uid').val() }
    if (sel) {
      const data = JSON.parse(template.formBuilder.formData)
      if (data.length > 0) {
        Meteor.call(
          'FormBuilder.dynamicForms.upsert', sel, data,
          (err) => {
            if (err) {
              alert(err)
            } else {
              // success!
              template.name = sel.name
            }
          },
        )
      } else {
        alert('Form empty, Nothing to save')
      }
    } else {
      alert('You must assign a machine name to the form')
    }
  },
})
