import SimpleSchema from 'simpl-schema';

var fs = Npm.require("fs");
assetsPath = process.env.PWD + "/private";

Meteor.methods({
  'FormBuilder.dynamicForms.upsert': function(sel,data) {
    doc = _.extend(sel,{form: data});
    console.log("FormBuilder.dynamicForms.upsert sel",sel.name);
    console.log("FormBuilder.dynamicForms.upsert data",{form: data});
    SimpleSchema.validate(doc, Schemas.DynamicForms);
    DynamicForms.upsert({name: sel.name},{$set: {form: data}});
  },

  'FormBuilder.dynamicForms.remove': function(name) {
    console.log("FormBuilder.dynamicForms.remove",name);
    check(name,String);
    DynamicForms.remove({name:name});
  },

  // 'FormBuilder.dynamicForms.save': function(name) {
  //   console.log("FormBuilder.dynamicForms.save",name);
  //   check(name,String);
  //   form = DynamicForms.findOne({name:name});
  //   file = "#{assetsPath}/#{name}.formbuilder.json";
  //   console.log("write #{file}");
  //   fs.writeFileSync(file, JSON.stringify(form));
  // }
});
