// This exports all dynamic forms. Maybe it would be a better idea to
// let the owner decide ...
Meteor.publish("FormBuilder.dynamicForms", function () {
  return DynamicForms.find();
});
